## Golang Build Container
FROM registry.gitlab.com/strasheim/go-container AS builder

WORKDIR /go/b
COPY . .
ENV GOARCH=amd64 GOOS=linux CGO_ENABLED=0
RUN go build -ldflags "-s -w -extldflags \"-static\""

## Application Container
FROM scratch 

COPY --from=builder /go/b/monsterjagt  /monsterjagt
COPY --from=builder /go/b/html/        /html/

USER 65534
EXPOSE 8000
CMD  ["/monsterjagt"]
