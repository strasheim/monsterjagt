// golang webserver, serving a directory of web content
package main

import (
	"log"
	"net/http"
	"regexp"
)

const pw1 = "1990"   // Gewicht vom Kuchen
const pw2 = "39"     // 3 Uhr und 9 Uhr
const pw3 = "347830" // Rainbow

func main() {
	http.HandleFunc("/passwords", passwords) // ajax
	http.Handle("/", http.FileServer(http.Dir("./html")))
	log.Println("Listening...")
	must(http.ListenAndServe(":8000", nil))
}

func passwords(w http.ResponseWriter, r *http.Request) {
	pass1 := r.FormValue("p1")
	pass2 := r.FormValue("p2")
	pass3 := r.FormValue("p3")

	// just in case some input checking
	xp := regexp.MustCompile(`^\d+$`)
	if !xp.MatchString(pass1) && xp.MatchString(pass2) && xp.MatchString(pass3) {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("alles Passwörter sind reine Zahlen")) // nolint -- where to report if not here
		return
	}

	// the actual pass checking, with some hints for the small once
	switch {
	case pass1 == pw1 && pass2 == pw2 && pass3 == pw3:
		// this is the correct answer
		return
	case pass1 == pw2 || pass1 == pw3 && pass2 == pw1 || pass2 == pw3 && pass3 == pw1 || pass3 == pw1, // all mixed
		pass1 == pw2 && pass2 == pw1 && pass3 == pw3, // 1 and 2 mixed
		pass1 == pw3 && pass3 == pw1 && pass2 == pw2, // 1 and 3 mixed
		pass1 == pw1 && pass2 == pw3 && pass3 == pw2: // 3 and 2 mixed
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("die Passwörter sind nicht in der richtigen Reihenfolge")) // nolint -- where to report if not here
		return
	default:
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("die Passwörter sind nicht richtig")) // nolint -- where to report if not here
	}
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
